package com.example.servicepoint.ui.home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.example.servicepoint.R;
import com.example.servicepoint.VisorImagen;

import static java.security.AccessController.getContext;

class Adaptador extends BaseAdapter {

    private static LayoutInflater inflater = null;

    FragmentActivity contexto;
    String[][] datos;
    int[] datosImg;


    public Adaptador(FragmentActivity activity, String[][] datos, int[] datosImg) {
        this.contexto = activity;
        this.datos = datos;
        this.datosImg = datosImg;

        inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final View vista = inflater.inflate(R.layout.elemento_lista, null);

        TextView titulo = (TextView) vista.findViewById(R.id.tituloImagen);
        TextView duracion = (TextView) vista.findViewById(R.id.direccion);
        TextView director = (TextView) vista.findViewById(R.id.valor);

        ImageView imagen = (ImageView) vista.findViewById(R.id.imagen22);
        RatingBar calificacion = (RatingBar) vista.findViewById(R.id.calificacion);

        titulo.setText(datos[i][0]);
        director.setText(datos[i][1]);
        duracion.setText("Duración " + datos[i][2]);
        imagen.setImageResource(datosImg[i]);
        calificacion.setProgress(Integer.valueOf(datos[i][3]));

        imagen.setTag(i);

     imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent visorImagen = new Intent(contexto, VisorImagen.class);
                visorImagen.putExtra("IMG", datosImg[(Integer)v.getTag()]);
                contexto.startActivity(visorImagen);
            }
        });


        return vista;

    }

    @Override
    public int getCount() {
        return datosImg.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

}
